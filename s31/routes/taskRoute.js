//Contains all the endpoints for our application
//We separate the routes such that "app.js" only contains information on the server.
const express = require("express");

//Create a Router instance that functions as a middleware and routing system.
	//This allows access to HTTP method middlewares that make it easier to create routes for our application.
const router = express.Router();

//Declare taskController
const taskController = require("../controllers/taskControllers");


//[ROUTES]
//[GET]=>Route for RETRIEVING all tasks
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => 
		res.send(resultFromController))
});//end of router.get


//[POST]=>Route for CREATING a task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => 
		res.send(resultFromController))
});//end of router.post


//[DEL]=>Route for DELETING a task.
	//Call out the routing component to register a brand new endpoint.
	//When integrating a path variable within the URI, you are also changing the behavior of the path from STATIC to DYNAMIC.
		//2 Types of Endpoints:
			//1. Static Route - unchanging, fixed or constant
			//2. Dynamic Route - interchangeable or not fixed
				//To make a route dynamic, just add colon (:)
router.delete('/:task', (req, res) => {
	//identify the task to be executed within this endpoint.
	//call out the proper function for this route & identify the source/provider of the function.
	//Determine whether the value inside the path variable is transmitted to the server.
	console.log(req.params.task);
	//Place the value of the path variable inside its own container.
	let taskDelId = req.params.task;
	//res.send('Hello from Delete'); //this is to check if the setup is correct.

	//Retrieve the identity of the task by inserting the ObjectId of the resource
	//Make sure to pass down the identity of the task using the proper reference(objectID), but this time we will include the information as a path variable.
	//Path variable -> this will allow us to insert data within the scope of the URL.
		//What is a variable? -> container, storage of information

	//When is a path variable useful? 
		//-> When inserting only a single piece of information.
		//-> Also useful when passing down information to REST API method that does not include a BODY section like 'GET'.

	/*Upon executing this method, a promise will be initialized so we need to handle the result of the promise in our route module.*/
	taskController.deleteTask(taskDelId).then(resultOfDelete => res.send(resultOfDelete));
});//end of router.delete


//[PUT]=>Route for UPDATING a task status (From: 'Pending' => "Complete")
router.put('/:task', (req, res) => {
	//Check if you can acquire the variables inside the path variables.
	console.log(req.params.task);
	let idOfTask = req.params.task; //This is declared to simplify the means of calling out the value of the path variable key.

	//Identify the business logic behind this task inside the controller module.
	//Call the intended controller to execute the process.
	//Handle the outcome by using the 'then' method.
	taskController.taskCompleted(idOfTask).then(outcome => res.send(outcome));
});//end of router.put (from pending to complete)


//[PUT]=>Route for UPDATING a task status (From: 'Complete' => "Pending")
	//This is a counter procedure of the previous task.
	//We added another endpoint 'pending' so that there wouldn't be any conflict from the previous router.put.
router.put('/:task/pending', (req, res) => {
	let id = req.params.task;
	//console.log(id); //test the initial setup

	//declare the business logic aspect of this brand new task in our application.
	//invoke using dot notation
	taskController.taskPending(id).then(outcome => {
		res.send(outcome);
	});
});//end of router.put (from complete to pending)



//[EXPORT MODULE]->we use module.exports to export the router object to use in the app.js
module.exports = router;