//Create the Schema, model
const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
}); //end of const taskSchema function

module.exports = mongoose.model("Task", taskSchema);