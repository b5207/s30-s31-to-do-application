//[SECTION DEPENDENCIES AND MODULES]
	const express = require("express");
	const mongoose = require("mongoose");
	const taskRoute = require("./routes/taskRoute");

//[SECTION SERVER]->Server Setup
	//establish server connection.
	const app = express();
	//assigning the port to 4000 to avoid conflict in future frontend applications that run on port 3000.
	const port = 4000;

//[SECTION DATABASE]->Database Connection
	//Connect to MongoDB Atlas
	mongoose.connect('mongodb+srv://theinquisitiveprogrammer:admin123@cluster0.qyhnu.mongodb.net/toDo176?retryWrites=true&w=majority', {

		//options to add to avoid deprecation warning because of mongoose/mongoDB update.
		useNewUrlParser:true,
		useUnifiedTopology: true
	});

//[SECTION MONGOOSE]->Create notification if the connection is successful or not.
	let db = mongoose.connection;
	//Let's add an on() method from our mongoose connection to show if the connection has succeeded or failed in both terminals and in the browser for our client.
	db.on('error', console.error.bind(console, "Connection Error"));

	//once the connection is successful, we will output a message in the terminal:
	db.once('open', ()=>console.log("Connected to MongoDB"));

	//Middleware - a middleware, in express.js context, are method, functions that act and add features to our application.
	app.use(express.json());
	app.use(express.urlencoded({extended: true}));


//[SECTION TASKROUTE]->Add the taskroute
	//localhost:4000/tasks/
	app.use("/tasks", taskRoute);



//[SECTION TERMINAL RESPONSE]->Entry Point Response
	//bind the connection to the designated port.
	app.listen(port, () => console.log(`Server running at port: ${port}`));
