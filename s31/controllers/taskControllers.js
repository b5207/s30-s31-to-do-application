//CONTROLLERS -> contain the functions and business logic of our Express JS Application.
const Task = require("../models/task");

//[SECTION] - [CONTROLLERS]
//[SECTION] - Create
	module.exports.createTask = (requestBody) => {
		let newTask = new Task({
			name: requestBody.name
		})
		return newTask.save().then((task, error) => {
			if(error){
				return false;
			} else {
				return task;
			}; //end of if-else
		})
	}; //end of module.exports.createTask function

//[SECTION] - Retrieve
	module.exports.getAllTasks = () => {
		return Task.find({}).then(result => {
			return result
		}); //end of return
	}; //end of module.exports.getAllTasks function

//[SECTION] - Update
	//TASK 1. Change the status of Task from pending => "Completed".
	module.exports.taskCompleted = (taskIdOfUpdate) => {
		//Search for the desired task to update.
		//The "findById()" mongoose method will look for a resource will look for a resource (task) that matchs the ID from the URL of the request.
		//A new promise will be instantiated from performing this method and we will use a 'then' method to handle its possible results.
		return Task.findById(taskIdOfUpdate).then((found, error)=>{
			//process the document found from the collection and change the status from 'pending' to 'completed'.
			if(found){
				//call out the parameter that describes the result of the search when successful.
				console.log(found);
				found.status = 'Completed';
				//we will chain a thenable expression upon executing a save() method into our returned document.

				return found.save().then((updatedTask, saveErr) =>{
					if(updatedTask) {
						return 'Task has been successfully updated.';
					} else {
						return 'Task failed to update. Try again.';
					}
				}); //end of return


			} else {
				return 'Error! No document found.';
			};//end of if-else (parent)

		});//end of return

	}; //end of module.exports.taskCompleted



	//TASK 2. Change the status of Task from completed => "Pending".
	module.exports.taskPending = (userInput) => {
		//Search the database for the user input to check if there's a match.
		//Assign & invoke this new controller task to its separate route.
		return Task.findById(userInput).then((result, err) => {
			if(result) {
				result.status = 'Pending';
				return result.save().then((taskUpdatedToPending, error) => {
					if(taskUpdatedToPending){
						return `Task: '${taskUpdatedToPending.name}' was modified to Pending.`;
					} else {
						return `An error occured while saving task updates.`;
					}
				});//end of return result.save()

			} else {
				return `Sorry, Server couldn't process your request.`;
			}
		});//end of return Task.findById
	}; //end of module.exports.taskPending



//[SECTION] - Destroy
	//1. Remove an existing resource inside the TASK collection.
	//Expose the data cross other modules other app so that it will become reusable.
	//Would we need an input from the user? => which resource you want to target

	module.exports.deleteTask = (taskId) => {
		//how will the data will be processes in order to execute the task.
		//select which mongoose method will be used in order to acquire the desired end goal.
		//Mongoose -> provides an interface and methods to manipulate the resources found inside the mongoDB atlas.

		//findByIdAndRemove => is a mongoose method that targets a document using its ID field and remove the targeted document from the collection.
			//Upon executing this method, a new promise will be instantiated upon waiting for the task to be waiting. However, we need to handle the outcome of the promise whatever state it may fall on.

		//PROMISES IN JS
			//=> Pending (waiting to be executed)
			//=> Fulfilled (successfully executed)
			//=> Rejected (unfulfilled promise)
		//We are going to insert a 'thenable' expression to determine HOW we will respond depending on the result of the promise.
		//Identify the 2 possible states of the promise using a THEN expression.
		return Task.findByIdAndRemove(taskId).then((fulfilled, rejected) => {
			if (fulfilled) {
				return 'The Task has been successfully removed.';
			} else {
				return 'Failed to remove Task';
			}; //end of if-else
			//assign a new endpoint for this route.
		});

	}; //end of module.exports.deleteTask


