//[SECTION A.] Dependencies and Modules
	const express = require("express");
	const mongoose = require("mongoose");
//[SECTION B.] Server Setup
	//establish server connection.
	const app = express();
	//assing the port to 4000 to avoid conflict in future frontend applications that run on port 3000.
	const port = 4000;

//[SECTION C.] Database Connection
	//Connect to MongoDB Atlas
	mongoose.connect('mongodb+srv://theinquisitiveprogrammer:admin123@cluster0.qyhnu.mongodb.net/toDo176?retryWrites=true&w=majority', {

		//options to add to avoid deprecation warning because of mongoose/mongoDB update.
		useNewUrlParser:true,
		useUnifiedTopology: true
	});

	//Create notification if the connection is successful or not.
	let db = mongoose.connection;
	//Let's add an on() method from our mongoose connection to show if the connection has succeeded or failed in both terminals and in the browser for our client.
	db.on('error', console.error.bind(console, "Connection Error"));

	//once the connection is successful, we will output a message in the terminal:
	db.once('open', ()=>console.log("Connected to MongoDB"));

	//Middleware - a middleware, in express.js context, are method, functions that act and add features to our application.
	app.use(express.json());

	/*[SCHEMA] - before we can create documents from our api to save in our database, we first have to determine the structure of the documents to be written in our database. This is to ensure the consistency of our documents to avoid future errors.

	Schema acts as a blueprint for our data/document. */

	//Schema() constructor from mongoose to create a new schema object
	const taskSchema = new mongoose.Schema({

		/* Define the fields for our documents.
		   We will also be able to determine the appropriate data type of the values.
		*/
		name: String,
		status: String
	});

	//Mongoose Model
	/*
		Models are used to connect your api to the corresponding collection in your database. It is a representation of your collection.

		Models use schemas to create objects that correspond to the schema. By default, when creating the collection from your model, the collection name is PLURALIZED.

		SYNTAX: mongoose.model(<nameOfCollectionInAtlas>, <schemaToFollow>)
	*/
	const Task = mongoose.model("task", taskSchema);

	//Post route to create a new task
	app.post('/tasks',(req,res)=>{
		/* When creating a new post/put or any route that requires data from the client, first console log your req.body or any part of the request that contains the data.
		*/
		//console.log(req.body);

		//Creating a new task document by using the constructor of our Task model. 
			//This constructor should follow the schema of the model.
		let newTask = new Task({
			name: req.body.name,
			status: req.body.status
		});

		//.save() method from an object created by a model.
		//save() method will allow us to save our document by connecting it to our collection via our model.

		//save() has 2 approaches:
		//1. We can add an anonymous function to handle the crated document or error.
		//2. We can add .then() chain which will allow us to handle errors and created documents in separate functions
		/*newTask.save((error, savedTask) => {
			if(error){
				res.send(error);
			} else {
				res.send(savedTask);
			}
		}); //end of newTask.save function*/


		//another way of error handling
		//.then() and .catch() chain
		//.then() is used to handle the returned value of a function. If the function properly returns a value, we can use a separate function to handle it.
		//.catch() is used to handle/catch the error thru the use of function. So that if an error occurs, we can handle the error separately.

		newTask.save()
		.then(result => res.send({message: "Document Creation Successful"}))
		.catch(error => res.send({message: "Error in Document Creation"}));

	}); //end of app.post('/tasks') function

	//[SECTION]-->GET Method to retrieve ALL task documents from our collection.
	app.get('/tasks', (req,res)=> {
		//To query using mongoose, first access the model of the collection you want to manipulate.
		//Model.find() in mongoose is similar in function to mongoDB's db.collection.find()
		//mongoDB - db.task.find({})
		Task.find({})
		.then(result => res.send(result))
		.catch(error => res.send(error));

	}) //end of app.get


const sampleSchema = new mongoose.Schema({
	name: String,
	isActive: Boolean
})

const Sample = mongoose.model("samples", sampleSchema);

app.post('/samples',(req, res)=>{

	let newSample = new Sample({
		name: req.body.name,
		isActive: req.body.isActive
	}); //end of let newSample

	newSample.save((error, savedSample)=>{
		if(error){
			res.send(error);
		} else {
			res.send(savedSample);
		}; //end of if-else statement

	}); //end of newSample.save

}); //end of app.post('/samples')

//[SECTION]-->GET Method to retrieve ALL sample documents from our collection.
app.get('/samples', (req,res)=> {
	Sample.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));

}) //end of app.get


//[ACTIVITY S30-S31]
//Schema() constructor 
	const userSchema = new mongoose.Schema({
		username: String,
		password: String
	});

	const User = mongoose.model("user", userSchema);

//POST route to create a new User.
	app.post('/users',(req,res)=>{

		let newUser = new User({
			username: req.body.username,
			password: req.body.password
		});

	newUser.save()
		.then(result => res.send({message: "Document was created successfully!"}))
		.catch(error => res.send({message: "There was an error in the creation of document. Please try again."}));

	}); //end of app.post('/users') function


//GET route to retrieve ALL user documents.
app.get('/users', (req,res)=> {
	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));

}) //end of app.get('/users')




//[SECTION D.] Entry Point Response
	//bind the connection to the designated port.
	app.listen(port, () => console.log(`Server running at port: ${port}`));
